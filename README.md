# RockMassClassification

## Sobre la aplicación

`RockMassClassification` está diseñada como herramienta para la valoración de macizos rocosos, puede servir como método de enseñanza o incluso como herramienta profesional (a futuro se ingresará nuevas funcionalidades). Actualmente cuenta con tres tipos de valoraciones geomecánicas: el **RMR89**, **RMR14** y el índice **Q**, por su amplio uso en geotecnia.

## Desarrollo y uso

La aplicación es desarrollado bajo el lenguaje `R`, basado principalmente en el paquete `shiny`. Por lo que, quien desee utilizar la app necesita instalar [R](https://cran.r-project.org/) y sus respectivas librerías. Para una mejor facilidad en correr el programa y revisar los scripts, se recomienda utilizar [RStudio](https://www.rstudio.com/) como IDE.
